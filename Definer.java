import java.util.ArrayList;
import java.util.Arrays;

public class Definer {
	public Definer() {
	}

	boolean isWhiteSpace(String c) {
		String whitespace[] = { " ", "\t", "\r", "\n", "\r\n" };
		return Arrays.asList(whitespace).contains(c);
	}

	boolean isDigit(String c) {
		if (Character.isDigit(c.charAt(0)) || c == "-") {
			return true;
		} else {
			return false;
		}
	}
}

class jObject extends ArrayList<jObject> {
	public jObject() {
//		System.out.println("Build jObject");
	}

	public void push(jObject s) {
		this.add(s);
	}
}

class jArray extends jObject {

	public jArray() {
//		System.out.println("Build jArray");
	}

	public void push(jObject jvalue) {
		this.add(jvalue);
	}
}

class jComponent extends jObject {
	jString key;
	jValue value;

	public jComponent() {
//		System.out.println("Build jComponent");
	}

	public jComponent(jComponent jcom) {
//		System.out.println("Copy jComponent");
		this.key = jcom.key;
		this.value = jcom.value;
	}
}

class jCharacter extends jObject {
	String content = "";

	public jCharacter() {
//		System.out.println("Build jCharacter");
	}

}

class jValue extends jObject {
	jObject content;

	public jValue() {
//		System.out.println("Build jValue");
	}
}

class jString extends jObject {
	String content = "";

	public jString() {
//		System.out.println("Build jString");
	}

	void addContent(String s) {
		this.content += s;
	}
}

class jNumber extends jObject {
	String content;

	public jNumber() {
//		System.out.println("Build jNumber");
	}
}

class jEmpty extends jString {
	String content;

	public jEmpty() {
//		System.out.println("Build jEmpty");
		this.content = "Empty";
	}
}