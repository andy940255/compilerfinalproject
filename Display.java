import java.util.ArrayList;
import java.util.Dictionary;
import java.util.HashMap;

public class Display {
	jObject res;

	Display(jObject result) {
		this.res = result;
		show();
	}

	void show() {
//		System.out.println("res: " + res);
		if (!(res instanceof jObject)) {
			dealException(res);
			return;
		}
		ArrayList tree = dealParsing(res);
		System.out.println("\n\nResult");
		System.out.println(tree.get(0));
	}

	ArrayList dealParsing(jObject node) {
		ArrayList result = null;
		if (node instanceof jArray) {
			result = new ArrayList<Object>();
			for (int i = 0; i < node.size(); i++) {
				jObject arraySecondLayer = node.get(i);// 打開最外層的{}
//				System.out.println(i + "-arraySecondLayer: " + arraySecondLayer);
//				System.out.println("Array State" + arraySecondLayer.getClass());
				result.add(dealArrayItem(arraySecondLayer));
			}
		} else if (node instanceof jObject) {
			result = new ArrayList<Dictionary>();
			for (int i = 0; i < node.size(); i++) {
				jObject objSecondLayer = node.get(i);// 打開最外層的{}
//				System.out.println(i + "-objSecondLayer: " + objSecondLayer);
//				System.out.println("Object State: " + objSecondLayer.getClass());
				result.add(dealObjectItem(objSecondLayer));
			}
		}
		return result;
	}

	private Object dealObjectItem(jObject node) {
		HashMap map = new HashMap();
		if (node instanceof jComponent) {
//			System.out.println("jComponent State");
			jComponent jcom = (jComponent) node;
			if (!(jcom.key instanceof jEmpty)) {
				String key = "\"" + jcom.key.content + "\"";
				Object value = dealValue(jcom.value.content);
//				System.out.println("Key: " + key + "\nValue: " + value);
//				System.out.println("Value: " + value.getClass());
				map.put(key, value);
			}

		} else if (node instanceof jObject || node instanceof jArray) {
			return dealParsing(node);
		}
		HashMap tempMap = new HashMap();
//		System.out.println("Map: " + map);
		tempMap.putAll(map);
		return tempMap;
	}

	private Object dealArrayItem(jObject node) {
		if (node instanceof jString) {
//			System.out.println("jString State");
			jString jstr = (jString) node;
			return dealString(jstr);
		} else if (node instanceof jNumber) {
//			System.out.println("jNumber State");
			jNumber jnum = (jNumber) node;
			return dealNumber(jnum);
		} else if (node instanceof jCharacter) {
//			System.out.println("jCharacter State");
			jCharacter jchar = (jCharacter) node;
			return dealChar(jchar);
		} else if (node instanceof jValue) {
//			System.out.println("jValue State");
			jValue jvalue = (jValue) node;
			return dealValue(jvalue.content);
		} else if (node instanceof jObject || node instanceof jArray) {
			return dealParsing(node);
		}
		return null;
	}

	private Object dealValue(jObject node) {
		if (node instanceof jString) {
//			System.out.println("jString State");
			jString jstr = (jString) node;
			return dealString(jstr);
		} else if (node instanceof jNumber) {
//			System.out.println("jNumber State");
			jNumber jnum = (jNumber) node;
			return dealNumber(jnum);
		} else if (node instanceof jCharacter) {
//			System.out.println("jCharacter State");
			jCharacter jchar = (jCharacter) node;
			return dealChar(jchar);
		} else if (node instanceof jObject || node instanceof jArray) {
			return dealParsing(node);
		}
		return null;
	}

	private String dealChar(jCharacter jchar) {
		return jchar.content;
	}

	private String dealNumber(jNumber jnum) {
		return jnum.content;
	}

	private String dealString(jString jstr) {
		return "\"" + jstr.content + "\"";
	}

	private Object dealException(jObject node) {
		if (node instanceof jString) {
			jString jstr = (jString) node;
			return dealString(jstr);
		} else if (node instanceof jNumber) {
			jNumber jnum = (jNumber) node;
			return dealNumber(jnum);
		} else if (node instanceof jCharacter) {
			jCharacter jchar = (jCharacter) node;
			return dealChar(jchar);
		} else if (node instanceof jObject || node instanceof jArray) {
			return dealParsing(node);
		}
		return null;
	}
}
