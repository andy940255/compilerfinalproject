import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class main {
	public static void main(String[] args) {
		FileReader fr;
		String input = "";
		try {
			fr = new FileReader("input.txt");
			BufferedReader br = new BufferedReader(fr);
			while (br.ready()) {
				input += br.readLine();
			}
			fr.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		Parser parser = new Parser(input);
		new Display(parser.parse());
	}
}
