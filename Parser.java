import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Parser {
	String input;
	int index;
	Definer definer = new Definer();

	public Parser(String input) {
		this.input = input;
		index = -1;
	}

	jObject parse() {
		jObject parsingResult = new jObject();
		while (true) {
			skipWhiteSpace();
			String c = nextChar();
			switch (c) {
			case "-1":
				return parsingResult;
			case "{":
				parsingResult.push(dealObject());
				c = lookhead();
				if (c.equals("}"))
					nextChar();
				else
					error("miss }");
				break;
			case "[":
				parsingResult.push(dealArray());
				c = lookhead();
				if (c.equals("]"))
					nextChar();
				else
					error("miss ]");
				break;
			case "\"":
				parsingResult.push(dealString());
				c = lookhead();
				if (c.equals("\""))
					nextChar();
				else
					error("miss \"");
				break;
			default:
				System.out.println("**Default - ERROR**");
				if (definer.isWhiteSpace(c)) {
					System.out.println("isWhiteSpace");
				} else if (definer.isDigit(c)) {
					System.out.println("isDigit");
				}
			}
			break;
		}
		return parsingResult;
	}

	private jValue dealValue() {
//		System.out.println("-------Deal dealValue");
		jValue jvalue = new jValue();
		String c = lookhead();
		if (c.equals("\"")) {
			nextChar();
			jvalue.content = dealString();
			c = lookhead();
			if (c.equals("\""))
				nextChar();
			else
				error("miss \"");
		} else if (definer.isDigit(c)) {
			jvalue.content = dealNumber();
		} else if (c.equals("{")) {
			nextChar();
			jvalue.content = dealObject();
			c = lookhead();
			if (c.equals("}"))
				nextChar();
			else
				error("miss }");
		} else if (c.equals("[")) {
			nextChar();
			jvalue.content = dealArray();
			c = lookhead();
			if (c.equals("]"))
				nextChar();
			else
				error("miss ]");
		} else {
			jvalue.content = dealMultiChar();
		}
		return jvalue;
	}

	private jCharacter dealMultiChar() {
//		System.out.println("-------Deal dealMultiChar");
		String c = lookhead();
		jCharacter jchar = new jCharacter();
		String buffer = "";
		while (c.matches("[a-z]")) {
			c = nextChar();
			buffer += c;
			c = lookhead();
		}
		switch (buffer) {
		case "true":
			jchar.content = "true";
			break;
		case "false":
			jchar.content = "false";
			break;
		case "null":
			jchar.content = "null";
			break;
		default:
			error("Unexpected Character ：" + buffer);
		}
		return jchar;
	}

	private jNumber dealNumber() {
//		System.out.println("-------Deal dealNumber");
		skipWhiteSpace();
		jNumber jnumber = new jNumber();

		String buffer = "";
		Pattern finalP = Pattern.compile("^([1-9][0-9]*|0)(.[0-9])?[0-9]*(e|E)?(-|\\+)?[0-9]*");
		Pattern tempP = Pattern.compile("[0-9+\\-Ee.]");
		while (true) {
			String c = lookhead();
			Matcher tempM = tempP.matcher(c);
			if (definer.isWhiteSpace(c)) {
				break;
			} else if (tempM.matches()) {
				c = nextChar();
				buffer += c;
			} else {
				break;
			}
		}
		Matcher finalM = finalP.matcher(buffer);
		if (finalM.matches()) {
			jnumber.content = buffer;
			return jnumber;
		} else {
			error("Number is not Conform to the format");
			return null;
		}
	}

	private jString dealString() {
		jString jstring = new jString();
		String buffer = "";
		while (true) {
			String c = lookhead();
			if (c.equals("\"")) {
				return jstring;
			} else if (c.equals("\\")) {
				nextChar();
				buffer += "\\";
				c = lookhead();
				String escapeChar[] = { "\"", "\\", "/", "b", "f", "n", "r", "t" };
				if (Arrays.asList(escapeChar).contains(c)) {
					c = nextChar();
					buffer += c;
				} else if (c.equals("u")) {
					c = nextChar();
					buffer += "u";
					String uChar = "";
					Pattern p = Pattern.compile("[0-9a-fA-F]");
					Matcher m;
					for (int i = 0; i < 4; i++) {
						c = nextChar();
						m = p.matcher(c);
						if (m.matches()) {
							uChar += c;
						} else {
							error("Unexpected Character after \\u");
						}
					}
					buffer += uChar;
				} else {
					error("unexpected character after \\");
				}
				jstring.addContent(buffer);
			} else {
				c = nextChar();
				jstring.addContent(c);
			}
		}

	}

	private jArray dealArray() {
//		System.out.println("-------Deal dealArray");
		jArray jarray = new jArray();
		jValue jvalue = new jValue();
		boolean checkEmpty = true;
		while (true) {
			skipWhiteSpace();
			String c = lookhead();
			if (c.equals("]")) {
				if (checkEmpty) {
					return jarray;
				}
				jarray.push(jvalue);
				return jarray;
			} else if (c.equals(",")) {
				nextChar();
				jarray.push(jvalue);
				continue;
			} else {
				checkEmpty = false;
				jvalue = dealValue();
			}
		}
	}

	private jObject dealObject() {
//		System.out.println("-------Deal dealObject");
		boolean stateFlag = false;
		boolean checkEmpty = true;
		jObject jobject = new jObject();
		jComponent jcomponent = new jComponent();
		while (true) {
			skipWhiteSpace();
			String c = lookhead();
			if (c.equals("}")) {
				if (checkEmpty) {
					jcomponent.key = new jEmpty();
				}
				jobject.push(new jComponent(jcomponent));
				return jobject;
			} else if (c.equals(",")) {
				stateFlag = false;
				nextChar();
				if (checkEmpty) {
					jcomponent.key = new jEmpty();
				}
				jobject.push(new jComponent(jcomponent));
				continue;
			} else if (c.equals("\"") && !stateFlag) {
				nextChar();
				jcomponent.key = dealString();
				c = lookhead();
				if (c.equals("\""))
					nextChar();
				else
					error("miss \"");
			} else if (c.equals(":")) {
				stateFlag = true;
				nextChar();
			} else {
				checkEmpty = false;
				jcomponent.value = dealValue();
			}
		}

	}

	String nextChar() {
		index += 1;
		if (index == input.length()) {
			return "-1";
		}
//		System.out.println("NextChar\nIndex: " + index + "\tChar: " + input.charAt(index));
		return Character.toString(input.charAt(index));
	}

	String lookhead() {
		index += 1;
		if (index == input.length()) {
			return "-1";
		}
//		System.out.println("LookHead\nIndex: " + index + "\tChar: " + input.charAt(index));
		return Character.toString(input.charAt(index--));
	}

	void error(String content) {
		System.out.println("**" + content + "**");
		System.exit(1);
	}

	private void skipWhiteSpace() {
		while (definer.isWhiteSpace(lookhead())) {
			nextChar();
		}
	}
}
